<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="header.jsp"></jsp:include>

</head>
<body class="text-center" style="align-items: center; height: 100%;">
	<c:import url="/navbar.jsp" />
	<div class="h-100 d-inline-block"></div>
	<div class="container">
		<form method="post" class="form-signin" action="/login"
			style="width: 100%; max-width: 330px; padding: 15px; margin: auto;">
			<img class="mb-4"
				src="https://jornal-t.pt/wp-content/themes/jornal-t/images/sociallogo.jpg"
				alt="" width="72" height="72">
			<h1 class="h3 mb-3 font-weight-normal">Por favor, faça login!</h1>
			<label for="login" class="sr-only">Nome de usuário</label> <input
				type="text" id="login" class="form-control" name="login"
				placeholder="Nome de usuário" required="" autofocus=""> <label
				for="password" class="sr-only">Senha</label> <input type="password"
				id="password" class="form-control" name="password"
				placeholder="Senha" required="">
			<div class="h-100 d-inline-block"></div>

			<button class="btn btn-lg btn-primary btn-block" type="submit"
				id="entrar" value="entrar">Entrar</button>
			Ainda não possui uma conta? Clique <a href="novousuario.jsp">aqui</a>
		</form>

		<%
			if (null != request.getParameter("erro")) {
				out.println("\n" + request.getParameter("erro"));
			}
		%>

	</div>

	<jsp:include page="scripts.jsp"></jsp:include>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>
</body>
</html>