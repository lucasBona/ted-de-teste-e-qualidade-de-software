<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="header.jsp" ></jsp:include>
    <jsp:useBean id="usuarios" class="java.util.ArrayList" scope="request" />
    <jsp:useBean id="usuario" class="br.ucsal.app.todo.model.Usuario" scope="request" />
</head>
<body>
<c:import url="/navbar.jsp" />

<div class="container">
    <h5>Bem vindo(a), ${usuario.nome}</h5>
    <c:if test="${usuario.administracao}"><p class="font-italic"> Contas de administrador podem alterar dados de usuários</p></c:if>
</div>

    <div>
        <c:import url="/userform.jsp" />
    </div>

    <div class="container">
    <hr>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseSenha" aria-expanded="false" aria-controls="collapseExample">
                Alterar sua senha
            </button>
        </p>
        <div class="collapse" id="collapseSenha">
            <div class="card card-body">
                <form method="post" action="/usuarios/mudasenha">
                    <input type="hidden" name="id" value="${usuario.id}" >
                    <div class="form-group">
                        <label for="senhaatual">Senha atual</label>
                        <input type="password" class="form-control" id="senhaatual" name="senhaatual">
                    </div>
                    <div class="form-group">
                        <label for="novasenha">Nova senha</label>
                        <input type="password" class="form-control" id="novasenha" name="novasenha">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    <hr>
    </div>

<c:if test="${usuario.administracao}">
    <div class="container">

        <table class="table table-striped ">
            <tr>
                <th>Login</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Ativo</th>
                <th>Tarefas</th>
                <th>ADM?</th>
                <th colspan="2">Quantidade de usuários: ${usuarios.size()} </th>

            </tr>
            <c:forEach var="user" items="${usuarios}" >
                <tr>
                    <td>${user.login}</td>
                    <td>${user.nome}</td>
                    <td>${user.email}</td>
                    <td>${user.ativo ? 'SIM' : 'NÃO'}</td>
                    <td>${user.tarefas.size()}</td>
                    <td>${user.administracao ? 'SIM' : 'NÃO'}</td>
                    <td><a href="/usuarios/excluir?index=${user.id}" >EXCLUIR</a></td>
                    <td><a href="/usuarios/editar?index=${user.id}" >ALTERAR</a></td>

                </tr>
            </c:forEach>
        </table>

    </div>
</c:if>




</body>
</html>