<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="header.jsp" ></jsp:include>
    <jsp:useBean id="tarefas" class="java.util.ArrayList" scope="request" />
    <jsp:useBean id="tarefascomp" class="java.util.ArrayList" scope="request" />
</head>
<body>
<c:import url="/navbar.jsp" />

<div class="container">

    <div>
        <c:import url="/tarefaform.jsp" />
    </div>
    <hr>

    <div>

        <h6>Minhas tarefas</h6>
        <table class="table table-striped ">
            <tr>
                <th>Titulo</th>
                <th>Descrição</th>
                <th>Concluida</th>
                <th colspan="2">Numero de Tarefas: ${tarefas.size()} </th>
                <th>Compartilhamento</th>

            </tr>
            <c:forEach var="task" items="${tarefas}" >
                <tr>
                    <td>${task.titulo}</td>
                    <td>${task.descricao}</td>
                    <td>${task.concluida ? 'SIM' : 'NÃO'}</td>
                    <td><a href="/excluir?index=${task.id}" >EXCLUIR</a></td>
                    <td><a href="/editar?index=${task.id}" >ALTERAR</a></td>

                    <td  class="align-middle text-center"><a class="btn btn-primary" data-toggle="collapse" href="#colapsar" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Compartilhar
                    </a><br>
                    <div class="collapse" id="colapsar">
                        <div class="card card-body">
                            <form method="get" action="/compartilhar"  >
                                <input type="hidden" name="index" id="index" value="${task.id}" >
                            Digite o nome de usuário de quem poderá ver esta tarefa
                                <input type="text" class="form-control" id="login" name="login">
                                <div align="right">
                                <button type="submit" class="btn btn-primary" >Enviar</button>
                                    <%
                                        if(null!=request.getParameter("mensagem")) {
                                            out.println("\n" + request.getParameter("mensagem"));
                                        }
                                    %>
                                </div>
                            </form>
                        </div>
                    </div>
                        <a href="/compartilharlistcontroller?index=${task.id}">DETALHES</a>
                    </td>

                </tr>


            </c:forEach>
        </table>
        <hr>
        <h6>Tarefas compartilhadas comigo - Total: ${tarefascomp.size()} </h6>
        <table class="table table-striped ">
            <tr>
                <th>Titulo</th>
                <th>Descrição</th>
                <th>Concluida</th>

            </tr>
            <c:forEach var="taskcomp" items="${tarefascomp}" >
                <tr>
                    <td>${taskcomp.titulo}</td>
                    <td>${taskcomp.descricao}</td>
                    <td>${taskcomp.concluida ? 'SIM' : 'NÃO'}</td>
                </tr>


            </c:forEach>
        </table>


    </div>

</div>

<jsp:include page="scripts.jsp" ></jsp:include>

</body>
</html>
