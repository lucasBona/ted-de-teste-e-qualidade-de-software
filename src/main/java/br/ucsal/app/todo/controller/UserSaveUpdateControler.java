package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.dao.Virus;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@WebServlet(urlPatterns = { "/usuarios/salvar", "/usuarios/editar"})
public class UserSaveUpdateControler extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UsuarioDAO udao = new UsuarioDAO();



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String index = request.getParameter("index");

        if( index != null ) {
            long id = Long.parseLong(index);
            Usuario usuario = udao.findById(id);
            request.setAttribute("usuario",usuario);
        }

        request.setAttribute("usuarios", udao.listAll());
        request.getRequestDispatcher("/usuarios").forward(request, response);


    }



    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");
        String login = request.getParameter("login");
        String senha = null;
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String ativo = request.getParameter("ativo");
        boolean novo = false;

        Usuario usuario = new Usuario();
        if(id!=null && !id.trim().isEmpty()) {
            long i = Long.parseLong(id);
            usuario.setId(i);
            senha = request.getParameter("senha");
        } else {
            try {
                senha = Virus.toPassword(request.getParameter("senha"));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            novo = true;
        }
        usuario.setSenha(senha);
        usuario.setLogin(login);
        usuario.setNome(nome);
        usuario.setEmail(email);
        usuario.setAtivo(ativo!=null?true:false);


        try {
            udao.saveOrUpdate(usuario);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if(novo) {
            response.sendRedirect("../index.jsp");
        } else {
            request.setAttribute("usuario", usuario);
            request.getRequestDispatcher("/usuarios.jsp").forward(request, response);
        }

    }
}
