package br.ucsal.app.todo.controller;


import br.ucsal.app.todo.dao.TarefaDAO;
import br.ucsal.app.todo.model.Compartilhamento;
import br.ucsal.app.todo.model.Tarefa;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/compartilharlistcontroller")
public class CopartilharListController extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("index");
        long i = Long.parseLong(id);
        TarefaDAO tdao = new TarefaDAO();
        Tarefa tarefa = tdao.findById(i);
        List<Compartilhamento> listcomp = tdao.listaCompartilhamento(tarefa);

        request.setAttribute("compartilhadocom", listcomp);
        request.setAttribute("tarefa", tarefa);

        request.getRequestDispatcher("compartilhar.jsp").forward(request, response);

    }
}
