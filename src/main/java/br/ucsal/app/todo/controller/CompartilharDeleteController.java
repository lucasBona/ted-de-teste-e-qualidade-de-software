package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.CompartilharDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deletecomp")
public class CompartilharDeleteController extends HttpServlet {

    private CompartilharDAO cdao = new CompartilharDAO();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String index = request.getParameter("index");
        if( index != null ) {
            try {
                long id = Long.parseLong(index);
                cdao.delete(id);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
        }
        response.sendRedirect("/tarefas");
    }
}
