package br.ucsal.app.todo.controller;

import br.ucsal.app.todo.dao.CompartilharDAO;
import br.ucsal.app.todo.dao.UsuarioDAO;
import br.ucsal.app.todo.model.Usuario;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/compartilhar")
public class CompartilharController extends HttpServlet {

    private CompartilharDAO cdao = new CompartilharDAO();
    private UsuarioDAO udao = new UsuarioDAO();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String index = request.getParameter("index");
        String login = request.getParameter("login");

        Usuario usuario = udao.findByUser(login);
        if(usuario == null) {
           request.getSession().setAttribute("mensagem", "Usuário não encontrado");
           response.sendRedirect("/tarefas");
        } else {
            Long usuario_id = usuario.getId();
            Long tarefa_id = Long.parseLong(index);

            cdao.save(usuario_id, tarefa_id);

            response.sendRedirect("/tarefas");
        }


    }
}
