package br.ucsal.app.todo.model;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
	 private Long id;

	    private String login;
	    private String nome;
	    private String email;
	    private String senha;
	    private Boolean ativo;
	    private Boolean administracao;
	    private List<Tarefa> tarefas = new ArrayList<>();

	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	public void setTarefas(List<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}

	public Long getId() {
	        return id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public String getLogin() {
	        return login;
	    }

	    public void setLogin(String login) {
	        this.login = login;
	    }

	    public String getNome() {
	        return nome;
	    }

	    public void setNome(String nome) {
	        this.nome = nome;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }

	    public String getSenha() {
	        return senha;
	    }

	    public void setSenha(String senha) {
	        this.senha = senha;
	    }

	    public Boolean getAtivo() {
	        return ativo;
	    }

	    public void setAtivo(Boolean ativo) {
	        this.ativo = ativo;
	    }

	public Boolean getAdministracao() {
		return administracao;
	}

	public void setAdministracao(Boolean administracao) {
		this.administracao = administracao;
	}
}
