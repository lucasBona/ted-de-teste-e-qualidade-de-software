package br.ucsal.app.todo.dao;


import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Virus {
    public static String toPassword(String pass) throws NoSuchAlgorithmException {
        //String hash = "35454B055CC325EA1AF2126E27707052";
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(pass.getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest);
        return myHash;
    }
}
