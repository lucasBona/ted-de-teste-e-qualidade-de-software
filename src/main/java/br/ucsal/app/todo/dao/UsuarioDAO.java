package br.ucsal.app.todo.dao;

import br.ucsal.app.todo.model.Tarefa;
import br.ucsal.app.todo.model.Usuario;
//import jdk.swing.interop.SwingInterOpUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO {

    private Context ctx = null;
    private DataSource ds = null;
    private Connection con = null;
    private static final String SELECT_ALL = "select USUARIO_ID, LOGIN, SENHA, NOME, EMAIL, ATIVO, ADMINISTRADOR from USUARIOS order by USUARIO_ID";
    private static final String SELECT_ALL_TAREFAS = "select TAREFA_ID, TITULO, DESCRICAO, CONCLUIDA, USUARIO_ID from TAREFAS";
    private static final String UPDATE = "UPDATE usuarios SET login=?, senha=?, nome=?,email=?, ativo=? where usuario_id=?";
    private static final String UPDATE_SENHA = "UPDATE usuarios SET senha=? where usuario_id=?";
    private static final String UPDATE_ATIVO = "UPDATE usuarios SET ativo=? where usuario_id=?";
    private static final String INSERT = "INSERT INTO usuarios ( login, senha, nome, email, ativo ) VALUES ( ?,?,?,?,? )";
    private static final String DELETE = "DELETE FROM usuarios where usuario_id=?";
    private static final String SELECT_BY_ID = "select USUARIO_ID, LOGIN, SENHA, NOME, EMAIL, ATIVO, ADMINISTRADOR from USUARIOS where USUARIO_ID =?";
    private static final String SELECT_BY_USER = "select USUARIO_ID, LOGIN, SENHA, NOME, EMAIL, ATIVO, ADMINISTRADOR from USUARIOS where LOGIN =?";

    public void open() throws Exception {
        ctx = new InitialContext();
        ds = (DataSource) ctx.lookup("jdbc/tarefasDS");
        con = ds.getConnection();
    }

    private void close(Statement stmt) {
        close(stmt,null);
    }

    private void close(Statement stmt, ResultSet rs ) {
        try {
            if(rs!=null) rs.close();
            if(stmt!=null)stmt.close();
            if(con!=null)con.close();
            if(ctx!=null)ctx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private void close(ResultSet rs ) {
        try {
            if(rs!=null) rs.close();
            if(con!=null)con.close();
            if(ctx!=null)ctx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }


    public List<Usuario> listAll() {
        List<Usuario> usuarios = new ArrayList<Usuario>();
        Statement stmt = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try {
            this.open();
            stmt = con.createStatement();
            rs = stmt.executeQuery(SELECT_ALL);

            Usuario usuario = new Usuario();
            while (rs.next()) {
                rs2 = stmt.executeQuery(SELECT_ALL_TAREFAS);
                Long id = rs.getLong("USUARIO_ID");
                if(usuario.getId() != id) {
                    usuario = new Usuario();
                    usuario.setId(rs.getLong("USUARIO_ID"));
                    usuario.setLogin(rs.getString("login"));
                    usuario.setNome(rs.getString("nome"));
                    usuario.setEmail(rs.getString("email"));
                    usuario.setSenha(rs.getString("senha"));
                    usuario.setAtivo(rs.getBoolean("ativo"));
                    usuario.setAdministracao(rs.getBoolean("ADMINISTRADOR"));
                }
                while (rs2.next()) {

                    if(usuario.getId() == rs2.getLong("usuario_id")) {
                        Tarefa tarefa = new Tarefa();
                        tarefa.setId(rs2.getLong("tarefa_id"));
                        tarefa.setTitulo(rs2.getString("titulo"));
                        tarefa.setDescricao(rs2.getString("descricao"));
                        tarefa.setConcluida(rs2.getBoolean("concluida"));
                        tarefa.setDono(usuario);
                        usuario.getTarefas().add(tarefa);
                    }
                }

                usuarios.add(usuario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.close(stmt, rs);
            this.close(rs2);
        }

        return usuarios;
    }

    public void update(Usuario usuario) {

        PreparedStatement stmt = null;

        try {
            this.open();
            stmt = con.prepareStatement(UPDATE);
            stmt.setString(1, usuario.getLogin());
            stmt.setString(2, usuario.getSenha());
            stmt.setString(3, usuario.getNome());
            stmt.setString(4, usuario.getEmail());
            stmt.setBoolean(5, usuario.getAtivo());
            stmt.setLong(6, usuario.getId());
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public void update_senha(Usuario usuario) {
        PreparedStatement stmt = null;
        try {
            this.open();
            stmt = con.prepareStatement(UPDATE_SENHA);
            stmt.setString(1,usuario.getSenha());
            stmt.setLong(2, usuario.getId());
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public void update_ativo(Usuario usuario) {
        PreparedStatement stmt = null;
        try {
            this.open();
            stmt = con.prepareStatement(UPDATE_ATIVO);
            stmt.setBoolean(1, usuario.getAtivo());
            stmt.setLong(2, usuario.getId());
            stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public void save(Usuario usuario) {
        PreparedStatement stmt = null;
        try {
            open();
            stmt = con.prepareStatement(INSERT);
            stmt.setString(1, usuario.getLogin());
            stmt.setString(2, usuario.getSenha());
            stmt.setString(3, usuario.getNome());
            stmt.setString(4, usuario.getEmail());
            stmt.setBoolean(5, usuario.getAtivo());
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public void delete(Long id) {
        Usuario usuario = new Usuario();
        usuario.setId(id);
        this.delete(usuario);
    }

    public void delete(Usuario usuario) {
        PreparedStatement stmt = null;

        try {
            open();
            stmt = con.prepareStatement(DELETE);
            stmt.setLong(1, usuario.getId());
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public Usuario findById(Long id) {
        Usuario usuario = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            open();
            stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                usuario = new Usuario();
                usuario.setId(rs.getLong("USUARIO_ID"));
                usuario.setLogin(rs.getString("LOGIN"));
                usuario.setNome(rs.getString("NOME"));
                usuario.setSenha(rs.getString("SENHA"));
                usuario.setEmail(rs.getString("EMAIL"));
                usuario.setAtivo(rs.getBoolean("ATIVO"));
                usuario.setAdministracao(rs.getBoolean("ADMINISTRADOR"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt, rs);
        }
        return usuario;
    }

    public Usuario findByUser(String user) {
        Usuario usuario = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            open();
            stmt = con.prepareStatement(SELECT_BY_USER);
            stmt.setString(1, user);
            rs = stmt.executeQuery();
            if (rs.next()) {
                usuario = new Usuario();
                usuario.setId(rs.getLong("USUARIO_ID"));
                usuario.setLogin(rs.getString("LOGIN"));
                usuario.setNome(rs.getString("NOME"));
                usuario.setSenha(rs.getString("SENHA"));
                usuario.setEmail(rs.getString("EMAIL"));
                usuario.setAtivo(rs.getBoolean("ATIVO"));
                usuario.setAdministracao(rs.getBoolean("ADMINISTRADOR"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt, rs);
        }
        return usuario;
    }

    public void saveOrUpdate(Usuario usuario) throws NoSuchAlgorithmException {
        if(usuario.getId()==null) {
            save(usuario);
        }else {
            update(usuario);
        }
    }
}