package br.ucsal.app.todo.dao;

import br.ucsal.app.todo.model.Compartilhamento;
import br.ucsal.app.todo.model.Tarefa;
import br.ucsal.app.todo.model.Usuario;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CompartilharDAO {
    private Context ctx = null;
    private DataSource ds = null;
    private Connection con = null;
    private static final String LIST_BY_ID = "select TAREFA_ID from COMPARTILHAMENTOS where USUARIO_ID = ?";
    private static final String INSERT = "insert into COMPARTILHAMENTOS (TAREFA_ID, USUARIO_ID) VALUES (?,?)";
    private static final String DELETE = "delete from COMPARTILHAMENTOS where COMPARTILHAMENTO_ID = ?";


    public void open() throws Exception {
        ctx = new InitialContext();
        ds = (DataSource) ctx.lookup("jdbc/tarefasDS");
        con = ds.getConnection();
    }

    private void close(Statement stmt) {
        close(stmt,null);
    }

    private void close(Statement stmt, ResultSet rs ) {
        try {
            if(rs!=null) rs.close();
            if(stmt!=null)stmt.close();
            if(con!=null)con.close();
            if(ctx!=null)ctx.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public List<Tarefa> listCompartilhado(String index) {
        List<Tarefa> tarefaCompartilhada = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        TarefaDAO dao = new TarefaDAO();
        try {
            this.open();
            stmt = con.prepareStatement(LIST_BY_ID);
            stmt.setString(1, index);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Long id = rs.getLong("tarefa_id");
                Tarefa tarefa = dao.findById(id);
                tarefaCompartilhada.add(tarefa);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt, rs);
        }

        return  tarefaCompartilhada;
    }

    public void save(long usuario_id, long tarefa_id) {
        PreparedStatement stmt = null;

        try {
            open();
            stmt = con.prepareStatement(INSERT);
            stmt.setLong(1, tarefa_id);
            stmt.setLong(2, usuario_id);
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

    public void delete(Long id) {
        Compartilhamento comp = new Compartilhamento();
        comp.setId(id);

        this.delete(comp);
    }

    public void delete(Compartilhamento compartilhamento) {
        PreparedStatement stmt = null;

        try {
            open();
            stmt = con.prepareStatement(DELETE);
            stmt.setLong(1, compartilhamento.getId());
            stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(stmt);
        }
    }

}
