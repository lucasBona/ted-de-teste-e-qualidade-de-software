package selenium;

import org.jbehave.core.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class LoginSteps {

    WebDriver driver;
    WebElement elememt;

    @BeforeScenario
    public void scenarioSetup() {
        System.setProperty("webdriver.chrome.driver", "http://localhost:8080");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(4l, TimeUnit.SECONDS);
    }

    /*
    Dado que estou na página inicial
Quando informo meu nome de usuário
E informo minha senha
E clico no botão entrar
Então sou levado à página de tarefas
     */

    @Given("estou na página inicial")
    public void testarLogin() {
        driver.get("http://localhost:8080/");
    }

    @When("informo meu nome de usuário $username")
    public void informaNomedeUsuario(String $username) {
        elememt = driver.findElement(By.id("login"));
        elememt.sendKeys($username);
    }

    @When("informo minha senha $senha")
    public void informaSenha(String $senha) {
        elememt = driver.findElement(By.id("password"));
        elememt.sendKeys($senha);
    }

    @When("clico no botão entrar")
    public void clicarBotaoEntrar() {
        driver.findElement(By.cssSelector("input[type='button'][value='entrar']")).click();
    }

    @Then("sou levado à página de tarefas")
    public void entreiNoSistema() {

    }

    @AfterScenario
    public void afterScenario() {
        driver.quit();
    }

}
