package builders;

import br.ucsal.app.todo.model.Tarefa;
import br.ucsal.app.todo.model.Usuario;

public class TarefaBuilder {


    private static final Long ID_DEFAULT = 001l;
    private static final String TITULO_DEFAULT = "Fazer o TED de Neiva";
    private static final String DESCRICAO_DEFAULT = "Não deixar para a última hora";
    private static final Boolean CONCLUIDA_DEFAULT = false;
    private static final Usuario DONO_DEFAULT = null;

    private Long id = ID_DEFAULT;
    private String titulo = TITULO_DEFAULT;
    private String descricao = DESCRICAO_DEFAULT;
    private Boolean concluida = CONCLUIDA_DEFAULT;
    private Usuario dono = DONO_DEFAULT;

    private TarefaBuilder() {
    }

    public static TarefaBuilder umaTarefa() {
        return new TarefaBuilder();
    }

    public TarefaBuilder comID(Long id) {
        this.id = id;
        return this;
    }

    public TarefaBuilder comTitulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public TarefaBuilder comDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public TarefaBuilder concluida(Boolean concluida) {
        this.concluida = concluida;
        return this;
    }

    public TarefaBuilder comDono(Usuario dono) {
        this.dono = dono;
        return this;
    }

    public TarefaBuilder mas() {
        return umaTarefa().comID(id).comTitulo(titulo).comDescricao(descricao).concluida(concluida).comDono(dono);
    }

    public Tarefa build() {
        Tarefa tarefa = new Tarefa();
        tarefa.setId(id);
        tarefa.setTitulo(titulo);
        tarefa.setDescricao(descricao);
        tarefa.setConcluida(concluida);
        tarefa.setDono(dono);

        return tarefa;
    }



}
